package com.codigopanda.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // esto es un comentario
        /*
        esto es un comentario
        mas largo de varias lineas
         */

        botonMultiplicar.setOnClickListener(View.OnClickListener {
            //Toast.makeText(this,"hola",Toast.LENGTH_LONG).show()
            //Log.e("error","error de datos")
            var number1:Int=Integer.parseInt(editNumber1.text.toString())
            var number2:Int=Integer.parseInt(editNumber2.text.toString())
            var mul:Int = number1*number2
            respuesta.text="Respuesta: ${mul.toString()}" // "respuesta "+mul.toString()
        })

        botonVerificar.setOnClickListener(View.OnClickListener {
            var number1:Int=Integer.parseInt(editNumber1.text.toString())
            if (number1 % 2==0)
                respuesta.text="Es Par"
            else
                respuesta.text="No es Par"
        })


        botonViewPrimo.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this,clsVerificarPrimo::class.java))
        })




    }
}
